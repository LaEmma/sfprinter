# -*- coding: utf-8 -*-
#!/usr/bin/env python
from settings import *
from utils.Database import DBSQLite
from utils.SQLs import *
from backend_sdk.backend import Backend
import logging

from zebra import zebra
from sfprint import sfprint

class printer():
    def __init__(self, printer_name, template_name):
        self.logger = logging.getLogger('main.printer')
        self.printername = printer_name #printer name
        self.templatename = template_name # ZPL template

    #retrive printing order list from local database    
    def get_printing_orders(self):
        #import pdb; pdb.set_trace()
        ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        sql = SELECT_ORDER
        result = ldb.ExecQuery(sql, (0,))
        li = []
        for i in result:
            li.append(i[0])
        return li

    #after printing, set order status to be printer = 1
    def update_order_status(self, order_id):
        ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        sql = UPDATE_STATUS
        result = ldb.ExecQuery(sql, (1,order_id))
        

    #extract order info which is needed for printing
    def get_printing_order_info(self, order_id):
        orderinfo = {}
        try:
            backend_api = Backend(app_id=APP_ID, app_secret=APP_SECRET)
            result = backend_api.get_express_order(order_id) 
            result2 = backend_api.get_order_by_sign(order_id)
            #construct reciver address  province + city + district + detail
            address = (result2['shipping_address']['province']
                    +result2['shipping_address']['city']
                    +result2['shipping_address']['district']
                    +result2['shipping_address']['detail'])
            
            reciver_addr = []
            n = len(address) / ADDR_LENGTH + 1
            i = 0
            #the following deals with multi-line address
            if n > 1:
                while i < n - 1 :
                    reciver_addr.append(address[i*ADDR_LENGTH:(i+1)*ADDR_LENGTH-1])
                    i += 1
            reciver_addr.append(address[i*ADDR_LENGTH:])

            #if there is child orders
            childList = []
            if result['child_orders']:
                for child_order in result['child_orders']:
                    childList.append(child_order['child_order_id'])

            orderinfo = {
                'barcode' : result['shipping_id'],
                'barcodeChild' : childList,
                'reciver' : result2['user'], # name, phone
                'reciver_addr' : reciver_addr,
                'sender' : result2['guide'], # name, phone
                'sender_addr' : '北京北京西单北大街176号汉光百货',
                'payment_method' : PAYMENT_METHOD,
                'payment_account' : PAYMENT_ACCOUNT,
                'destination_code' : result['express_profile']['destination_code'],
                'express_type' : result['express_profile']['express_type'],
                'service_type' : SERVICE_TYPE[result['express_profile']['express_type']],
            }
        except:
            #print("failed to get order detailed info")
            self.logger.error('faild to get printing order info')
        return orderinfo

    #convert to zpl language
    def prepare_content(self, orderinfo, isChild, sf):
        #import pdb; pdb.set_trace()
        content = sf.zpl_content(orderinfo, isChild)
        return content

    #connect to zebra printer and send zpl to printer
    def to_printer(self, outstream):
        zp = zebra(self.printername)
        zp.output(outstream)

    def print_all(self):
        try:
            sf = sfprint(self.templatename)
            #called once in the beginning
            orderList = self.get_printing_orders()
            #import pdb; pdb.set_trace()
            for order_id in orderList:
                orderinfo = self.get_printing_order_info(order_id)
                # if there is child orders, print child orders first; then parent order
                if orderinfo['barcodeChild']: # with child orders
                    count = 0
                    for child_order in orderinfo['barcodeChild']:
                        count += 1;
                        content = self.prepare_content(orderinfo, count, sf)
                        self.to_printer(content)
                        #self.updateOrderStatus(order_id)
                        #wait for 1 sec
                content = self.prepare_content(orderinfo, 0, sf)
                self.to_printer(content)
                self.update_order_status(order_id)
        except:
            #print('errors happened during printing')
            self.logger.error('error happened during printing')

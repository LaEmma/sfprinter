# -*- coding: utf-8 -*-
#!/usr/bin/env python
from settings import *
from utils.Database import DBSQLite
from utils.SQLs import *
from backend_sdk.backend import Backend
import time

from zebra import zebra
from sfprint import sfprint


class ZplConverter():


    def setBarcode(barcode, mx, my, rx, ry):
	//条码样式模板 Code128-C
        zpl = "^BY4,%d,%d^FT%d,%d^BCN,,N,N^FD>;%s^FS\n" % (mx, my, rx, ry, barcode)

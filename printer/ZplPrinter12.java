package printer.zebra;

import java.io.File;  
import java.io.FileInputStream;  
import java.io.IOException;  
import java.io.UnsupportedEncodingException;  
  
import javax.print.Doc;  
import javax.print.DocFlavor;  
import javax.print.DocPrintJob;  
import javax.print.PrintException;  
import javax.print.PrintService;  
import javax.print.PrintServiceLookup;  
import javax.print.SimpleDoc;  
import javax.print.attribute.standard.PrinterName;  
  
public class ZplPrinter12 {  
    private String printerURI = null;//打印机完整路径  
    private PrintService printService = null;//打印机服务  
    private byte[] dotFont;  
    private String begin = "";   //标签格式以^XA开始  
    private String end = "^XZ";     //标签格式以^XZ结束  
    private String content = "";  
  
    //public static void main(String[] args) {  
     //ZplPrinter p = new ZplPrinter("\\\\192.168.0.12\\ZDesigner 105SLPlus-300dpi ZPL");   
	//System.out.println(content);
	//ZplPrinter zpl = new ZplPrinter();
	//zpl.setText("我", 380, 320, 60, 60, 30, 1, 1, 22);  
	//String str1 = zpl.getZpl();
	//System.out.println(str1);
	
   // }  
  
    /** 
     * 构造方法 
     * @param printerURI 打印机路径 
     */  
    public ZplPrinter12(){  
        this.printerURI = printerURI;  
        //加载字体  
        File file = new File("C://HZK12");  
        if(file.exists()){  
            FileInputStream fis;  
            try {  
                fis = new FileInputStream(file);  
                dotFont = new byte[fis.available()];  
                fis.read(dotFont);  
                fis.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }else{  
            System.out.println("C://HZK16.lib文件不存在");  
        }  
 
    }  

    /**
    * 设置条形码
    * @param barcode 条码字符
    * @param zpl 条码样式模板
    */
    public void setBarcode(String barcode, int mx, int my, int rx, int ry) {
	//条码样式模板 Code128-C
	String zpl = String.format("^BY4,%d,%d^FT%d,%d^BCN,,N,N^FD>;${data}^FS\n", mx, my, rx, ry);  
	content += zpl.replace("${data}", barcode);
    }
  
    /** 
     * 中文字符、英文字符(包含数字)混合 
     * @param str 中文、英文 
     * @param x x坐标 
     * @param y y坐标 
     * @param eh 英文字体高度height 
     * @param ew 英文字体宽度width 
     * @param es 英文字体间距spacing 
     * @param mx 中文x轴字体图形放大倍率。范围1-10，默认1 
     * @param my 中文y轴字体图形放大倍率。范围1-10，默认1 
     * @param ms 中文字体间距。
     */  
    //the following load Chinese 12X12 dot matrix info  12X12点阵字库
    public void setText(String str, int x, int y, int eh, int ew, int es, int mx, int my, int ms) {  
        byte[] ch = str2bytes(str);  
        for (int off = 0; off < ch.length;) {  
            if (off < ch.length && ((int) ch[off] & 0x00ff) >= 0xA0) {  
            	try { 
	                int qcode = ch[off] & 0xff;  
	                int wcode = ch[off+1] & 0xff;  
                content += String.format("^FO%d,%d^XG0000%01X%01X,%d,%d^FS\n", x, y, qcode, wcode, mx, my);  
                begin += String.format("~DG0000%02X%02X,00024,002,\n", qcode, wcode);  
	                qcode = (qcode - 128 - 32) & 0x00ff;  
	                wcode = (wcode - 128 - 32) & 0x00ff;  
	                int offset = (int)( qcode -1 ) * 94 * 24 + (int)( wcode - 1 ) * 24;  
	                for (int j = 0; j < 24; j += 2) {  
                    qcode = (int) dotFont[j + offset] & 0x00ff;  
                    wcode = (int) dotFont[j + offset + 1] & 0x00ff;  
                    begin += String.format("%02X%02X\n", qcode, wcode ); 
                }  
	                x = x + ms * mx;  
	                off = off + 2; 
            	}
            	catch(Exception e) {  
                    e.printStackTrace();  
                    //替换成X号  
                    setChar("X", x, y, eh, ew);  
                    x = x + es;//注意间距更改为英文字符间距  
                    off = off + 2; 
            	}
            } else if (((int) ch[off] & 0x00FF) < 0xA0) {  
                setChar(String.format("%c", ch[off]), x, y, eh, ew);  
                x = x + es;  
                off++;  
            }  
        }  
    }  
    public void setChar(String str, int x, int y, int h, int w) { 
        content += "^FO" + x + "," + y + "^A0," + h + "," + w + "^FD" + str + "^FS";  
    }  

    /** 
     * 英文字符(包含数字)顺时针旋转90度 
     * @param str 英文字符串 
     * @param x x坐标 
     * @param y y坐标 
     * @param h 高度 
     * @param w 宽度 
     */  
    public void setCharR(String str, int x, int y, int h, int w) {  
        content += "^FO" + x + "," + y + "^A0R," + h + "," + w + "^FD" + str + "^FS";  
    }  

    /**
     * 获取完整的ZPL
     * @return
     */
    public String getZpl() {
	//return begin + content + end;
	return begin + content;
    }
    /** 
     * 字符串转byte[] 
     * @param s 
     * @return 
     */  
    public byte[] str2bytes(String s) {  
        if (null == s || "".equals(s)) {  
            return null;  
        }  
        byte[] abytes = null;  
        try {  
            abytes = s.getBytes("gb2312");  
        } catch (UnsupportedEncodingException ex) {  
            ex.printStackTrace();  
        }  
        return abytes;  
    }  
} 
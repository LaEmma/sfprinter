# -*- coding: utf-8 
#!/usr/bin/env python
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
from zebra import zebra
from jinja2 import Template
from cStringIO import StringIO
from settings import PAYMENT_METHOD, PAYMENT_ACCOUNT
import logging

class sfprint():
    def __init__(self, templatename):
        self.logger = logging.getlogger('main.sfprint')
        self.templatename = templatename

    def read_template(self):
        # load fixed ZPL commands
        try:
            f = open(self.templatename, "r")
            labelFixed = f.read()
            f.close()
            return labelFixed
        except:
            self.logger.error('cannot open zpl template file')
        
    def zpl_content(self, orderinfo, isChild):
        #import pdb;pdb.set_trace()
        # call java function
        try:
            z24 = JPackage('printer.zebra').ZplPrinter24 
            zebra24 = z24()
            z16 = JPackage('printer.zebra').ZplPrinter16 
            zebra16 = z16()

            if orderinfo['express_type'] == '2':
                zebra16.setText('E', 450, 20, 80, 80, 40, 2, 2, 18) #service type

            zebra16.setText(orderinfo['service_type'], 570, 110, 25, 25, 12, 2, 2, 18) #service type

            zebra24.setChar(orderinfo['destination_code'], 64, 255, 80, 80) #destination code
            zebra24.setText(orderinfo['reciver']['name'], 64, 348, 25, 25, 12, 1, 1, 22) #reciver name
            zebra24.setChar(orderinfo['reciver']['phone'], 304, 348, 30, 30) #reciver phone
            i = 0
            for address in orderinfo['reciver_addr']:
                zebra24.setText(address, 64, 378+i*30, 25, 25, 12, 1, 1, 22) #reciver address
                i += 1

            zebra24.setText(orderinfo['sender']['guide_name'], 64, 449, 20, 20, 10, 1, 1, 22) #sender name
            zebra24.setChar(orderinfo['sender']['phone'], 304, 449, 30, 30) #sender phone
            zebra24.setText(orderinfo['sender_addr'], 64, 477, 25, 25, 12, 1, 1, 22) #sender address

            zebra16.setText(orderinfo['payment_method'], 105, 548, 25, 25, 12, 1, 1, 16) #payment method
            zebra16.setChar(orderinfo['payment_account'], 105, 572, 20, 20) #payment account

            zebra24.setText(orderinfo['sender']['guide_name'], 64, 837, 25, 25, 12, 1, 1, 22) #sender name
            zebra24.setChar(orderinfo['sender']['phone'], 304, 837, 25, 25)#sender phone
            zebra24.setText(orderinfo['sender_addr'], 64, 862, 25, 25, 12, 1, 1, 22) #sender address

            zebra24.setText(orderinfo['reciver']['name'], 64, 922, 25, 25, 12, 1, 1, 22) #reciver name
            zebra24.setChar(orderinfo['reciver']['phone'], 304, 922, 25, 25) #reciver phone

            i = 0
            for address in orderinfo['reciver_addr']:
                zebra24.setText(address, 64, 947+i*25, 25, 25, 12, 1, 1, 22) #reciver address
                i += 1

            if not isChild:
                zebra24.setBarcode(orderinfo['barcode'], 3, 79, 80, 194)
                zebra16.setText(orderinfo['barcode'], 150, 200, 40, 40, 25, 1, 1, 24)
                zebra24.setBarcode(orderinfo['barcode'], 3, 72, 290, 792)
                zebra16.setText(orderinfo['barcode'], 360, 796, 40, 40, 25, 1, 1, 24)
            else:
                zebra16.setBarcode(orderinfo['barcodeChild'][isChild-1], 3, 82, 116, 184)
                zebra16.setText("子单号", 130, 198, 25, 25, 20, 1, 1, 24)
                zebra16.setText(orderinfo['barcodeChild'][isChild-1], 200, 194, 30, 30, 20, 1, 1, 24)
                zebra16.setText("母单号", 130, 222, 25, 25, 20, 1, 1, 24)
                zebra16.setText(orderinfo['barcode'], 200, 221, 30, 30, 20, 1, 1, 24)
                zebra16.setBarcode(orderinfo['barcodeChild'][isChild-1], 3, 64, 304, 780)
                zebra16.setText("子单号", 325, 786, 25, 25, 20, 1, 1, 24)
                zebra16.setText(orderinfo['barcodeChild'][isChild-1], 405, 783, 25, 25, 20, 1, 1, 24)
                zebra16.setText("母单号", 325, 810, 25, 25, 20, 1, 1, 24)
                zebra16.setText(orderinfo['barcode'], 405, 809, 25, 25, 20, 1, 1, 24)

            label = zebra24.getZpl() + zebra16.getZpl()
            labelFixed = self.read_template()
            # jinja2 render
            template = Template(labelFixed)
            t2 = template.render(teststring = label)
            return t2
        except:
            self.logger.error('failed to construct zpl content')
            return null
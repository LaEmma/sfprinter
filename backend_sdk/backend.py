# -*- coding: utf-8 -*-
import requests
import string
import random
import hashlib
import time
from base import BackendBase
from requests.exceptions import HTTPError
from settings import *
import logging

logger = logging.getLogger(__name__)


# BACKEND_BASE = "http://127.0.0.1:8001"
# BACKEND_BASE = "http://backend.dongyouliang.com"


class Backend(BackendBase):

    def __init__(self, app_id, app_secret):
        self._app_id = app_id
        self._app_secret = app_secret

    def build_url(self, url):
        url_list = []
        if IS_TEST_ENV:
            url_list.append(BACKEND_BASE_TEST)
        else:
            url_list.append(BACKEND_BASE)
        url_list.append(url)
        return ''.join(url_list)

    def get_access_token(self, user_id):
        '''
        @user_id: 用户手机号，或者登录名称
        '''
        uri = "/api/auth/user_id/"
        url = self.build_url(uri)
        data = {
            'user_id': user_id,
            'app_id': self._app_id,
            'app_secret': self._app_secret,
            'nonce': self._generate_random_string(7),
            'timestamp': int(time.time()),
        }
        sign = self._generate_signature(data)
        data.pop("app_secret")
        data['sign'] = sign
        result = requests.post(url, json=data)
        return self._handle_response(result=result, uri=url, arg_data=data)

    def _generate_signature(self, data):
        """生成签名"""
        keys = data.keys()
        keys.sort()
        # data_str = '&'.join(['%s=%s' % (key, data[key]) for key in keys])
        from utils.utils import convert_unicode_to_string
        data_str = '&'.join(['%s=%s' % (key, convert_unicode_to_string(data[key]) if not isinstance(data[key], list) else ",".join(convert_unicode_to_string(data[key]))) for key in keys])
        # signature = hashlib.md5(data_str.encode('utf-8')).hexdigest().upper()
        signature = hashlib.md5(data_str).hexdigest().upper()
        return signature

    def _generate_random_string(self, size=11, chars=string.ascii_uppercase + string.digits):
        # import pdb; pdb.set_trace()
        return ''.join(random.choice(chars) for _ in range(size))

    def get_user_self_guide(self, access_token):
        '''获取当前登录用户导购信息，如果登录用户不是导购，怎返回{}'''
        uri = '/api/guide/self/detail/'
        url = self.build_url(uri)
        # data = {}
        # import pdb; pdb.set_trace()
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        try:
            result.raise_for_status()
        except HTTPError:
            return {}
        return self._handle_response(result=result, uri=url)

    def get_guide_by_user_id(self, access_token, shop_id, user_id):
        uri = "/api/guide/detail/%s/%s/"
        uri = uri % (shop_id, user_id)
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def delete_guide_by_user_id(self, access_token, shop_id, user_id):
        uri = "/api/guide/delete/"
        url = self.build_url(uri)
        data = {
            'shop_id': shop_id,
            'user_id': user_id,
        }
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, json=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def add_guide(self, access_token, shop_id, phone, guide_name, is_manager):
        uri = "/api/guide/add/"
        url = self.build_url(uri)
        data = {
            'shop_id': shop_id,
            'phone': phone,
            'name': guide_name,
            'is_manager': is_manager,
        }
        # import pdb; pdb.set_trace()
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, json=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def set_current_shop(self, access_token, shop_id):
        '''设置当前专柜'''
        uri = "/api/guide/shop/set/"
        url = self.build_url(uri)
        data = {
            'shop_id': shop_id,
        }
        # import pdb; pdb.set_trace()
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, json=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_guide_shop_list(self, access_token):
        '''获取导购的专柜列表：多专柜支持'''
        uri = "/api/guide/shop/list/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_guide_list(self, access_token, shop_id):
        uri = '/api/guide/list/%s/'
        uri = uri % shop_id
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        # return self._handle_response(result=result, uri=url)
        return result.json()

    def get_user_id_by_union_id(self, union_id):
        '''
        @user_id: 用户手机号，或者登录名称
        '''
        uri = "/api/service/user/union_id/"
        url = self.build_url(uri)
        data = {
            'union_id': union_id,
        }
        result = requests.get(url, params=data)
        return self._handle_response(result=result, uri=url, arg_data=data)

    def send_sms_code(self, phone, created_user=False):
        '''
        @phone: 发送短信的手机号码
        created_user: 是否发送短信的同时创建以手机号为用户名的用户
        '''
        uri = "/api/sms/send/"
        url = self.build_url(uri)
        data = {
            "phone": phone,
            "created_user": created_user
        }
        result = requests.post(url, data=data)
        return self._handle_response(result=result, uri=url, arg_data=data)

    def verify_sms_code(self, phone, sms_code, created_user=False):
        '''
        @phone: 手机号
        @sms_code: 短信验证码
        @created_user: 是否创建以手机号为用户名的用户
        '''
        uri = "/api/sms/validate/"
        url = self.build_url(uri)
        data = {
            "phone": phone,
            "code": sms_code,
            "created_user": created_user
        }
        result = requests.post(url, data=data)
        return self._handle_response(result=result, uri=url, arg_data=data)

    def bind_wechat_account(self, access_token, union_id, open_id, extra_info=""):
        uri = "/api/account/binding/wechat/new/"
        url = self.build_url(uri)
        data = {
            "union_id": union_id,
            "open_id": open_id,
            "extra_info": extra_info
        }
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, data=data, headers=header)
        return self._handle_response(result=result, uri=url, arg_data=data)

    def get_product_list_no_base64(self, access_token):
        '''获取登录用户所在店铺的商品列表非 base64'''
        uri = "/api/product/offline/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_product_list(self, access_token):
        '''获取登录用户所在店铺的商品列表'''
        # import pdb; pdb.set_trace()
        uri = "/api/product/offline/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_statistic_home(self, access_token):
        uri = "/api/service/statistics/home/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_order_list(self, access_token, pay_status=None, created_time=None, sales_return_status=None):
        '''
        获取登录导购的订单列表:
        sales_return_status: 已退款 0: 未退货 1: 退货中 2: 已退货
        pay_status         : 0 未支付, 1 正在支付 2 已支付 3 完成支付 -1 支付失败 -2 订单主动关闭 -3 订单被动关闭
        created_time       : 创建日期 例子 2016-03-03
        '''
        # uri = "/api/offline/order/suborder/list/"
        uri = "/api/offline/order/new/order/list/"
        url = self.build_url(uri)
        logger.debug(url)
        data = {}
        if pay_status:
            data.update({"pay_status": pay_status})
        if created_time:
            data.update({"created_time__date": created_time})
        if sales_return_status:
            data.update({"refund_status": sales_return_status})
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, params=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_shop_order_list(self, access_token, pay_status=None, created_time=None, sales_return_status=None):
        '''
        获取品牌的订单列表:
        sales_return_status: 已退款 0: 未退货 1: 退货中 2: 已退货
        pay_status         : 0 未支付, 1 正在支付 2 已支付 3 完成支付 -1 支付失败 -2 订单主动关闭 -3 订单被动关闭
        created_time       : 创建日期 例子 2016-03-03
        '''
        # uri = "/api/offline/order/suborder/shop/list/"
        uri = "/api/offline/order/new/order/shop/list/"
        url = self.build_url(uri)
        logger.debug(url)
        data = {}
        if pay_status:
            data.update({"pay_status": pay_status})
        if created_time:
            data.update({"created_time__date": created_time})
        if sales_return_status:
            data.update({"refund_status": sales_return_status})
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, params=data, headers=header)
        # print data
        return self._handle_response(result=result, uri=url)

    def get_pre_order_list(self, access_token, status=None):
        # uri = "/api/offline/order/preorder/list/"
        uri = "/api/offline/order/new/preorder/createlist/"
        url = self.build_url(uri)
        data = {}
        if status is not None:
            data.update({"status": status})
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, params=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_pre_order(self, pre_order_id):
        uri = "/api/offline/order/new/preorder/detail/%s/"
        uri = uri % pre_order_id
        url = self.build_url(uri)
        # header = {'Authorization': 'Token %s' % (access_token, )}
        # result = requests.get(url, headers=header)
        result = requests.get(url)
        return self._handle_response(result=result, uri=url)

    def get_product_by_code(self, access_token, bar_code):
        '''根据商品二维码获取商品'''
        uri = "/api/product/offline_base64/%s/"
        uri = uri % bar_code
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def create_product(self, access_token, data):
        '''创建商品'''
        uri = "/api/product/offline_base64/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, data=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def create_pre_order(self, access_token, data):
        '''创建预售订单'''
        uri = "/api/offline/order/new/preorder/createlist/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, json=data, headers=header)
        # import pdb; pdb.set_trace()
        return self._handle_response(result=result, uri=url)

    def update_pre_order(self, access_token, pre_order_id, data):
        uri = "/api/offline/order/new/preorder/detail/%s/"
        uri = uri % pre_order_id
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.patch(url, data=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def update_product(self, access_token, bar_code, data):
        '''更新商品'''
        uri = "/api/product/offline_base64/%s/"
        uri = uri % bar_code
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.put(url, data=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_special_offer_list(self, access_token):
        '''获取中分类'''
        uri = "/api/service/special_offer/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def create_order(self, access_token, data):
        '''创建订单'''
        uri = "/api/offline/order/new/order/create/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, json=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def order_pay(self, order_id, data):
        # import pdb; pdb.set_trace()
        '''
        订单支付
        {
            "pay_type": "",
            "payment_amount": "",
            "pay_num": "",
            "confirmed": false
        }
        '''
        uri = "/api/offline/order/new/order/pay/%s/"
        uri = uri % order_id
        url = self.build_url(uri)
        timestamp = int(time.time())
        nonce = self._generate_random_string(7)
        app_id = settings.BACKEND_APP_ID
        secret = settings.BACKEND_APP_SECRET
        data_sign = {
            "app_id": app_id,
            "app_secret": secret,
            "nonce": nonce,
            "timestamp": timestamp,
        }
        data_sign.update(data)
        sign = self._generate_signature(data_sign)
        payload = {
            "app_id": app_id,
            "sign": sign,
            "nonce": nonce,
            "timestamp": timestamp,
        }
        payload.update(data)
        logger.info(payload)
        result = requests.put(url, json=payload)
        return self._handle_response(result=result, uri=url)

    def get_c_coupons(self, access_token, pre_order_id):
        '''c券'''
        uri = "/api/service/new/coupons/available/c/"
        url = self.build_url(uri)
        data = {
            "pre_order_id": pre_order_id
        }
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, json=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_gift_coupons(self, access_token, pre_order_id):
        '''获取礼券'''
        uri = "/api/service/new/coupons/available/gift/"
        url = self.build_url(uri)
        data = {
            "pre_order_id": pre_order_id
        }
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.post(url, data=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_order_by_sign(self, order_id):
        '''不登录，以签名的方式获取订单'''
        # import pdb; pdb.set_trace()
        timestamp = int(time.time())
        nonce = self._generate_random_string(7)
        app_id = self._app_id
        secret = self._app_secret
        data = {
            "order_id": order_id,
            "app_id": app_id,
            "app_secret": secret,
            "nonce": nonce,
            "timestamp": timestamp,
        }
        sign = self._generate_signature(data)
        # url = self.build_url('/api/offline/order/suborder/detail/httpsign/')
        url = self.build_url('/api/offline/order/new/sign/order/detail/')
        payload = {
            "order_id": order_id,
            "sign": sign,
            "app_id": app_id,
            "nonce": nonce,
            "timestamp": timestamp,
        }
        res = requests.post(url, json=payload)
        # import pdb; pdb.set_trace()
        respon_data = self._handle_response(result=res, uri=url)
        return respon_data

    def get_order(self, access_token, order_id):
        '''获取订单'''
        uri = "/api/offline/order/new/order/detail/%s/"
        uri = uri % order_id
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def update_order_payment_status(self, access_token, order_id, pay_status):
        uri = "/api/offline/order/new/order/pay/status/%s/"
        uri = uri % order_id
        url = self.build_url(uri)
        data = {
            "pay_status": pay_status,
        }
        header = {'Authorization': 'Token %s' % (access_token, )}
        # import pdb; pdb.set_trace()
        result = requests.put(url, data=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_statistic_by_day(self, access_token, m_day):
        uri = "/api/service/statistics/shop/"
        url = self.build_url(uri)
        data = {
            "date": m_day,
        }
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, params=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_customer_order_list(self, access_token, pay_status=None):
        '''
        从客户的角度去他的订单列表
        '''
        uri = '/api/offline/order/suborder/user/list/'
        url = self.build_url(uri)
        data = {}
        if pay_status is not None:
            data['pay_status'] = pay_status

        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, params=data, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_wechat_info(self, access_token):
        uri = '/api/service/user/wechat/detail/'
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_user_mamber_detail(self, access_token):
        uri = "/api/service/user/member/"
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def get_order_pay_status(self, access_token, order_id):
        '''获取订单支付状态'''
        uri = "/api/offline/order/new/order/pay/status/%s/"

        uri = uri % order_id
        url = self.build_url(uri)
        header = {'Authorization': 'Token %s' % (access_token, )}
        result = requests.get(url, headers=header)
        return self._handle_response(result=result, uri=url)

    def send_sms_notification(self, **kwargs):
        '''发送短信通知'''
        # data = {
        #     "phones": phones,
        #     "content": content,
        #     "is_backend": is_backend,
        #     "app_id": app_id,
        #     "app_secret": secret,
        #     "nonce": nonce,
        #     "timestamp": timestamp,
        # }
        timestamp = int(time.time())
        nonce = self._generate_random_string(7)
        app_id = settings.BACKEND_APP_ID
        secret = settings.BACKEND_APP_SECRET
        data = kwargs
        data.update({
            "app_id": app_id,
            "app_secret": secret,
            "nonce": nonce,
            "timestamp": timestamp,
        })
        # import pdb; pdb.set_trace()
        sign = self._generate_signature(data)
        url = self.build_url('/api/sms/content/')
        payload = kwargs
        payload.update({
            "sign": sign,
            "app_id": app_id,
            "nonce": nonce,
            "timestamp": timestamp,
        })
        res = requests.post(url, json=payload)
        # import pdb; pdb.set_trace()
        respon_data = self._handle_response(result=res, uri=url)
        return respon_data

    def get_express_order(self, order_id):
        '''路由信息也从这里取'''
        uri = "/api/shipping/order_shipping/%s/"
        uri = uri % order_id
        url = self.build_url(uri)
        # header = {'Authorization': 'Token %s' % (access_token, )}
        # result = requests.get(url, headers=header)
        result = requests.get(url)
        return self._handle_response(result=result, uri=url)

    def get_printing_order_list(self):
        ''' 获取待打印订单列表 '''
        uri = "/api/shipping/print/status/list/"
        url = self.build_url(uri)
        timestamp = int(time.time())
        nonce = self._generate_random_string(7)
        app_id = self._app_id
        secret = self._app_secret
        data = {}
        data = {
            "app_id": app_id,
            "app_secret": secret,
            "nonce": nonce,
            "timestamp": timestamp,
        }
        sign = self._generate_signature(data)
       
        data = {
            "sign": sign,
            "app_id": app_id,
            "nonce": nonce,
            "timestamp": timestamp,
        }
        res = requests.post(url, json=data)
        respon_data = self._handle_response(result=res, uri=url)
        return respon_data

    def create_sf_express_order_http_sign(self, order_id):
        timestamp = int(time.time())
        nonce = self._generate_random_string(7)
        app_id = settings.BACKEND_APP_ID
        secret = settings.BACKEND_APP_SECRET
        input_data = {
            'order_id': order_id
        }
        data = input_data
        data.update({
            "app_id": app_id,
            "app_secret": secret,
            "nonce": nonce,
            "timestamp": timestamp,
        })
        # import pdb; pdb.set_trace()
        sign = self._generate_signature(data)
        url = self.build_url('/api/shipping/sf_express_order_http_sign/')
        payload = input_data
        payload.update({
            "sign": sign,
            "app_id": app_id,
            "nonce": nonce,
            "timestamp": timestamp,
        })
        res = requests.post(url, json=payload)
        respon_data = self._handle_response(result=res, uri=url)
        return respon_data

    def create_sf_express_child_order_http_sign(self, order_id, quantity):
        timestamp = int(time.time())
        nonce = self._generate_random_string(7)
        app_id = settings.BACKEND_APP_ID
        secret = settings.BACKEND_APP_SECRET
        input_data = {
            'order_id': order_id,
            'quantity': quantity
        }
        data = input_data
        data.update({
            "app_id": app_id,
            "app_secret": secret,
            "nonce": nonce,
            "timestamp": timestamp,
        })
        # import pdb; pdb.set_trace()
        sign = self._generate_signature(data)
        url = self.build_url('/api/shipping/sf_express_child_order_http_sign/')
        payload = input_data
        payload.update({
            "sign": sign,
            "app_id": app_id,
            "nonce": nonce,
            "timestamp": timestamp,
        })
        res = requests.post(url, json=payload)
        # import pdb; pdb.set_trace()
        respon_data = self._handle_response(result=res, uri=url)
        return respon_data
# -*- coding: utf-8 -*-

from enum import Enum


class SalesReturnStatusType(Enum):
    '''
    退化状态
    '''

    RETURNING         = 1   # 退货中
    NOT_RETURN        = 0   # 未退货 
    RETURNED          = 2   # 已退货


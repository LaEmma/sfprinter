# -*- coding: utf-8 -*-
from enum import Enum

# 1   现金
# 2   支票
# 3   提货单
# 4   购物券
# 5   抵用券
# 6   应收帐
# 7   信用卡1
# 8   找零
# 9   信用卡2
# 10  e卡A券
# 11  连心卡
# 12  福卡
# 13  C券
# 14  回馈卡C券
# 15  D券
# 16  礼券
# 17  微信支付
# 18  支付宝

class PreOrderStatus(Enum):
    '''预售订单状态'''

    SALE_OUT       = 0  # 下架
    ONSELL         = 1  # 在售


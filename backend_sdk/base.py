# -*- coding: utf-8 -*-
from exceptions import BackendAPIException


class BackendBase(object):

    def _handle_response(self, result, uri, arg_data={}):
        result.raise_for_status()
        json_data = result.json()
        # 检查返回结果
        # if "code" in json_data and json_data["code"] != 0:
        #     raise BackendAPIException(json_data=json_data, uri=uri, data=arg_data)
        return json_data

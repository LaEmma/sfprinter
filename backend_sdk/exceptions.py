# -*- coding: utf-8 -*-


def convert_unicode_to_string(value):
    if isinstance(value, unicode):
        return value.encode('utf-8')
    return value


class BackendException(Exception):
    """
    微信 base Error
    """
    pass


class BackendAPIException(BackendException):

    def __init__(self, json_data, uri, data={}):
        self.json_data = json_data
        self.uri = uri
        self.data = data
        super(BackendAPIException, self).__init__(str(self))

    def __str__(self):
        message = 'code=%s, message=%s, api uri=%s, params=%s. '
        return message % (self.json_data['code'], convert_unicode_to_string(self.json_data['message']), self.uri, self.data)

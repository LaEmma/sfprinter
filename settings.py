# -*- coding: utf-8 -*
import sys, os
reload(sys)
sys.setdefaultencoding("utf-8")

#zebra printer settings
PRINTER_NAME = 'Zebra GK888d - EPL'
TEMPLATE_NAME = os.path.join(os.getcwd(),'printer', 'sftemplate.prn')
PAYMENT_METHOD = "寄付月结"
PAYMENT_ACCOUNT = "0104971724"
SERVICE_TYPE = {
    '1' : '顺丰次日',
    '2' : '顺风隔日',
    '5' : '顺风次晨',
    '6' : '顺风即日',
}
ADDR_LENGTH = 33

IS_TEST_ENV = True
BACKEND_BASE_TEST = "http://backend.dongyouliang.com"


DATABASES_SQLITE3 = {
    'DB_FILE_NAME': os.path.join(os.getcwd(),'orderlist.sqlite3')
}

APP_ID = "app_1470024182"
APP_SECRET = "594ba6bd949147b88bfc1980dd7024f4ff61374b"

# -*- coding: utf-8 -*-
import sqlite3
from settings import *
from utils.Database import DBSQLite
from utils.InitDBSQLite3 import InitDBSQLite3
from utils.WaybillDatabase import WaybillDatabase
from utils.SQLs import *
from backend_sdk.backend import Backend
from printer.printer import printer
from jpype import *

from multiprocessing import Process
import time

def JVM_on():
        jvmpath = getDefaultJVMPath()  
        startJVM(jvmpath, "-ea", "-Djava.class.path=.")  
    
def JVM_off():
        shutdownJVM()

def save_orders_to_database(wb):
    print('start to save orders to local database...')
    while True:
        wb.clean_db_data()
        li = wb.get_order_list()
        if li:
            wb.upload_data(li)
        time.sleep(10)

def send_orders_to_printer(sf):
    print('start to print orders...')
    while True:
        sf.print_all()
        time.sleep(10)

if __name__ == "__main__":
    import pdb;pdb.set_trace()
    InitDBSQLite3()
    wb = WaybillDatabase()
    p1 = Process(target=save_orders_to_database, args=(wb,))
    p1.start()
    #called once in the beginning
    JVM_on()
    sf = printer(PRINTER_NAME, TEMPLATE_NAME)
    p2 = Process(target=send_orders_to_printer, args=(sf,))
    p2.start()
    p2.join()
    JVM_off()
    p1.join()

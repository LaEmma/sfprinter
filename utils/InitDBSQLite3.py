# -*- coding: utf-8 -*-

'''
    Init SQLite3 when the program running firstly
'''
from SQLs import *
from Database import DBSQLite
from settings import DATABASES_SQLITE3
from utils import getDateTimeNow
import logging

INIT_DATABASE = [
    'create table if not exists orders(order_id varchar(50) primary key, status integer)',
]


class InitDBSQLite3():
    def __init__(self):
        self.logger = logging.getLogger('main.InitDBSQLite3')
        self.ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        self.__createDB()


    def __createDB(self):
        for s in INIT_DATABASE:
            self.ldb.ExecNoQuery(s, ())
        self.ldb.commit()
        self.logger.info('finished creating db')






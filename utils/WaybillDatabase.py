# -*- coding: utf-8 -*-
import sqlite3
from settings import *
from Database import DBSQLite
from SQLs import *
from backend_sdk.backend import Backend
import logging

class WaybillDatabase():
    def __init__(self):
        self.logger = logging.getLogger('main.WaybillDatabase')

    def clean_db_data(self):
        ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        sql = DELETE_ORDER
        ldb.ExecQuery(sql, (1,))
        ldb.commit()
        return True

    def get_order_list(self):
        #get orders need to be printed
        #get info from backend_sdk
        orderList = []
        try:
            backend_api = Backend(app_id=APP_ID, app_secret=APP_SECRET)
            orderList = backend_api.get_printing_order_list()
            return orderList['order_list']
        except:
            self.logger.error('failed to get order list from backend api')
            return orderList

    def upload_data(self, orderList):
        ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
        sql = INSERT_ORDER
        for order in orderList:
            ldb.ExecQuery(sql, (order, 0))
        ldb.commit()
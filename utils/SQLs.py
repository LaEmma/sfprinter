# -*- coding: utf-8 -*-
INSERT_ORDER = "insert into orders values(?, ?)"

SELECT_ORDER = 'select order_id from orders where status=?'

DELETE_ORDER = 'delete from orders where status=?'

UPDATE_STATUS = 'update orders set status=? where order_id=?'

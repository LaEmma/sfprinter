# -*- coding: utf-8 -*-

import base64
from Database import DBSQLite
from settings import DATABASES_SQLITE3
import uuid
from datetime import datetime


def encodestring(s):
    return base64.encodestring(s)


def decodestring(s):

    return base64.decodestring(s)


def getUUID():
    return uuid.uuid1()


# is firstly running
def isFirstlyRun():
    # import pdb; pdb.set_trace()
    ldb = DBSQLite(DATABASES_SQLITE3['DB_FILE_NAME'])
    re = ldb.ExecQuery('select count(*) from status', ())
    if(not re):
        return True
    if (re[0][0] < 1):
        return True
    else:
        return False


def toUnicode(string):
    return unicode(string)



#把datetime转成字符串  
def datetimeToString(dt):
    return dt.strftime('%Y-%m-%d %H:%M:%S')  
  

#把字符串转成datetime  
def stringToDatetime(string):
    return datetime.strptime(string, '%Y-%m-%d %H:%M:%S')  
  

#把字符串转成时间戳形式  
def string_toTimestamp(strTime):
    return time.mktime(string_toDatetime(strTime).timetuple())  
  

#把时间戳转成字符串形式  
def timestamp_toString(stamp):
    return time.strftime("%Y-%m-%d-%H", time.localtime(stamp))  
  

#把datetime类型转外时间戳形式  
def datetime_toTimestamp(dateTime):
    return time.mktime(dateTim.timetuple()) 


def getDateTimeNow():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def convert_unicode_to_string(value):
    if isinstance(value, unicode):
        # return simplejson.dumps(value, ensure_ascii=False).encode('utf8')
        return value.encode('utf-8')
    return value
